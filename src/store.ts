import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import { CellPosition, GameState, StateType } from '@/types'
import { Cells } from '@/types/cells'
import { detectCollisions } from '@/utilities/collision'
import { detectEnd } from '@/utilities/end'
import { graduate } from '@/utilities/graduate'
import { shuffleBoard } from '@/utilities/shuffle'

Vue.use(Vuex)

export default new Vuex.Store<StateType>({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    cells: new Cells(),
    chooseNumberFor: null,
    gameState: GameState.Halted,
    editMode: false
  },
  mutations: {
    startNewGame(state: StateType, data: { level: number }) {
      state.cells = graduate(shuffleBoard(), data.level)
      state.gameState = GameState.Running
      router.push('/board')
    },
    changeGameState(state: StateType, newState: GameState) {
      state.gameState = newState
      if (newState === GameState.Halted) {
        router.push('/')
      }
    },
    editMode(state: StateType, data: boolean) {
      state.editMode = data
    },
    chooseNumber(state: StateType, data: CellPosition) {
      state.chooseNumberFor = data
    },
    numberChosen(state: StateType, data) {
      if (state.chooseNumberFor) {
        const newCells = state.cells.clone()
        const cell = newCells.getCell(state.chooseNumberFor)
        if (state.editMode) {
          if (cell.notes.has(data.number)) {
            cell.notes.delete(data.number)
          } else {
            cell.notes.add(data.number)
          }
        } else {
          cell.number = data.number
          detectCollisions(newCells)
          if (detectEnd(newCells)) {
            state.gameState = GameState.Finished
            router.push('finished')
          }
        }
        state.cells = newCells
        state.chooseNumberFor = null
      }
    }
  }
})
