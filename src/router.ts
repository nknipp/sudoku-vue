import Vue from 'vue'
import Router from 'vue-router'
import Board from '@/views/board.vue'
import Game from '@/views/game.vue'
import Finished from '@/views/finished.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'game',
      component: Game,
    }, {
      path: '/board',
      name: 'board',
      component: Board
    }, {
      path: '/finished',
      name: 'finished',
      component: Finished
    }
  ]
})
