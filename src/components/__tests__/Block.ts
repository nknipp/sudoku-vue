import { shallowMount } from '@vue/test-utils'
import Block from '../Block.vue'

it('simply test it', () => {
  const wrapper = shallowMount(Block, { propsData: { blockIndex: 1 } })

  expect(wrapper.html()).toContain('<div class="block">')
})
