import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Cell from '../Cell.vue'
import { Cells } from '@/types/cells'

const localVue = createLocalVue()
localVue.use(Vuex)

let mutations: any
let store: any

beforeEach(() => {
  mutations = {
    chooseNumber: jest.fn()
  }

  store = new Vuex.Store({
    state: {
      cells: new Cells()
    },
    mutations
  })
})

it('vuex store', () => {
  const wrapper = mount(Cell, {
    store,
    localVue,
    propsData: { blockIndex: 0, cellIndex: 4 }
  })

  const cell = wrapper.find('div.cell')
  cell.trigger('click')

  expect(mutations.chooseNumber).toHaveBeenCalled()
})
