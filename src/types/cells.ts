import { Cell } from '@/types/cell'
import { CellPosition, CellState } from '@/types'
import { getIndexFromCellPosition } from '@/utilities/helpers'

export class Cells {
  private cells: Cell[]

  constructor() {
    this.cells = Array(81)
    for (let idx = 0; idx < this.cells.length; idx++) {
      this.cells[idx] = new Cell()
    }
  }

  get length() {
    return this.cells.length
  }

  public indexOf(cell: Cell) {
    return this.cells.indexOf(cell)
  }

  public forEach(func: (cell: Cell, index: number, array?: Cell[]) => void) {
    this.cells.forEach(func)
  }

  public filter(func: (cell: Cell) => boolean): Cell[] {
    return this.cells.filter(func)
  }

  public get(idx: number): Cell {
    return this.cells[idx]
  }

  public getCell(pos: CellPosition): Cell {
    const idx = getIndexFromCellPosition(pos)
    return this.cells[idx]
  }

  public clone(): Cells {
    const newCells = new Cells()
    newCells.cells = this.cells.slice(0)
    return newCells
  }
}
