import { CellState } from '@/types'

export class Cell {
  public number: number | null
  public notes: Set<number>
  public state: CellState

  constructor() {
    this.number = null
    this.notes = new Set()
    this.state = CellState.None
  }
}
