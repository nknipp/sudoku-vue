import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.directive('click-outside', {
  bind(el, binding, vnode) {
    (el as any).clickOutsideEvent = (event: Event) => {
      if (!(el === event.target || el.contains(event.target as Node))) {
        // @ts-ignore
        vnode.context[binding.expression](event)
      }
    }
    document.body.addEventListener('click', (el as any).clickOutsideEvent)
  },
  unbind(el) {
    document.body.removeEventListener('click', (el as any).clickOutsideEvent)
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
