import store from '../src/store'

store.commit('startNewGame',  { level: 3 })

export default previewComponent => {
  // https://vuejs.org/v2/guide/render-function.html
  return {
    store,
    render(createElement) {
      return createElement(previewComponent)
    }
  }
}
