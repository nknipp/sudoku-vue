/// <reference types="Cypress" />

it('should display the game page', () => {
  cy.visit('/')

  cy.contains('Start new game')
  cy.contains('select', 'Very easy')
})

it('should navigate to the board', () => {
  cy.visit('/')

  cy.contains('Start new game').click()
})